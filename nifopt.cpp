//
// Change bodypart number of Skyrim SE NIF with Outfit Studio NIFLIB
//
#include "NifFile.h"
#include "Geometry.h"
#include <string>

void ChangeExtension(std::string& filename, const std::string& extension)
{
	std::string::size_type i = filename.rfind('.', filename.length());

	if (i != std::string::npos) {
		filename.replace(i, extension.length(), extension);
	}
}

void changeBodyPart(NifFile& nif, uint32_t targetId, uint32_t replacementId)
{
	//NiHeader hdr = nif.GetHeader();
	
	for (auto& shape : nif.GetShapes()) {
		std::vector<BSDismemberSkinInstance::PartitionInfo> partitionInfo;
		std::vector<int> triParts;
		std::string shapeName = shape->GetName();
		fprintf(stdout, "Shape Name: %s\n", shapeName.c_str());
		nif.GetShapePartitions(shape, partitionInfo, triParts);
		for (int i = 0; i != partitionInfo.size(); i++) {
			if (partitionInfo[i].partID == targetId) {
				partitionInfo[i].partID = replacementId;
				nif.SetShapePartitions(shape, partitionInfo, triParts, false);
				fprintf(stdout, "  partId[%d]: %04x to %04x \n", i, targetId, replacementId);
			}
			else {
				fprintf(stdout, "  partId[%d]: %04x\n", i, partitionInfo[i].partID);
			}
		}
		
	}
}

int main(int argc, char* const argv[], char* const envp[])
{
	int partId1, partId2;
	if (argc != 4) {
		std::cout << 
			"Change bodypart number of Skyrim SE NIF with Outfit Studio NIFLIB \n\n"
			"Usage: nifopt.exe your.nif [targetPartId in decimal] [replacementPartId in decimal] " << std::endl;
		return 1;
	}

	NifFile nif;

	char* filename = argv[1];
	nif.Load(filename);

	NiHeader& hdr = nif.GetHeader();
	NiVersion& version = hdr.GetVersion();
	printf("NiVersion file: 0x%08x user: %u stream: %u\n", version.File(), version.User(), version.Stream());

	partId1 = strtol(argv[2], NULL, 10);
	partId2 = strtol(argv[3], NULL, 10);

	if (version.File() != 0x14020007) {
		printf("not supported.\n");
		return 0;
	}
	changeBodyPart(nif, partId1, partId2);

	std::string out_filename = std::string(filename);
	ChangeExtension(out_filename, ".mod.nif");

	printf("Save to %s\n", out_filename.c_str());
	nif.Save(out_filename.c_str());

	return 0;
}
